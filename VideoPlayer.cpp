#include "VideoPlayer.h"

IGraphBuilder * VideoPlayer::pGraph;
IMediaControl * VideoPlayer::pControl;
IMediaEventEx   * VideoPlayer::pEvent;
IVideoWindow  * VideoPlayer::pVidWin;
bool VideoPlayer::play = false;
bool VideoPlayer::IsPlay() 
{
	return play;
}
void VideoPlayer::Play(HWND hwnd, string name)
{
	CoCreateInstance(CLSID_FilterGraph, NULL, CLSCTX_INPROC, IID_IGraphBuilder, (void **)&pGraph);
	WCHAR wFileName[MAX_PATH];
	MultiByteToWideChar(CP_ACP, 0, name.c_str(), -1, wFileName, MAX_PATH);
	pGraph->RenderFile(wFileName, NULL);
	// Specify the owner window.
	pGraph->QueryInterface(IID_IVideoWindow, (void **)&pVidWin);
	pVidWin->put_Owner((OAHWND)hwnd);
	pVidWin->put_WindowStyle(WS_CHILD | WS_CLIPSIBLINGS);

	// Set the owner window to receive event notices.
	pGraph->QueryInterface(IID_IMediaEventEx, (void **)&pEvent);
	pEvent->SetNotifyWindow((OAHWND)hwnd, WM_GRAPH_EVENT, NULL);

	// Run the graph.
	pGraph->QueryInterface(IID_IMediaControl, (void **)&pControl);
	pControl->Run();
	play = true;
	RECT rect;
	if (GetWindowRect(hwnd, &rect))
	{
		int width = rect.right - rect.left;
		int height = rect.bottom - rect.top;
		Resize(width, height);
	}

}
void VideoPlayer::Resize(int width, int height) 
{
	if (!pVidWin) return;
	pVidWin->put_Width(width);
	pVidWin->put_Height(height);
	pVidWin->SetWindowPosition(0, 0, width, height);
}
void VideoPlayer::Stop() 
{
	pVidWin->put_Visible(OAFALSE);
	pVidWin->put_Owner(NULL);

	// Stop the graph.
	pControl->Stop();
	play = false;

	// Enumerate the filters in the graph.
	IEnumFilters *pEnum = NULL;
	HRESULT hr = pGraph->EnumFilters(&pEnum);
	if (SUCCEEDED(hr))
	{
		IBaseFilter *pFilter = NULL;
		while (S_OK == pEnum->Next(1, &pFilter, NULL))
		{
			pGraph->RemoveFilter(pFilter);
			pEnum->Reset();
			pFilter->Release();
		}
		pEnum->Release();
	}

	pControl->Release();
	pEvent->Release();
	pGraph->Release();
}

void VideoPlayer::HandleEvent(GraphEventFN f)
{
	if (!pEvent)
	{
		return;
	}

	long evCode = 0;
	LONG_PTR param1 = 0, param2 = 0;

	HRESULT hr = S_OK;

	// Get the events from the queue.
	while (SUCCEEDED(pEvent->GetEvent(&evCode, &param1, &param2, 0)))
	{
		// Invoke the callback.
		f(NULL, evCode, param1, param2);

		// Free the event data.
		hr = pEvent->FreeEventParams(evCode, param1, param2);
		if (FAILED(hr))
		{
			break;
		}
	}
}
//---------------------------------------------------------------------------
