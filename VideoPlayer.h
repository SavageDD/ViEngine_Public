#pragma once
#include <string>
#include <dshow.h>
using namespace std;
const UINT WM_GRAPH_EVENT = WM_APP + 1;
typedef void (CALLBACK *GraphEventFN)(HWND hwnd, long eventCode, LONG_PTR param1, LONG_PTR param2);

class VideoPlayer
{
	static IGraphBuilder * pGraph;    //Интерфейс менеджера графа-фильтров
	static IMediaControl * pControl;  //Интерфейс управления
	static IMediaEventEx   * pEvent;    //Интерфейс сообщений
	static IVideoWindow  * pVidWin;
	static bool play;
public:
	VideoPlayer() {};
	~VideoPlayer() {};
	static bool IsPlay();
	static void Play(HWND hwnd, string name);
	static void Resize(int width, int height);
	static void Stop();
	static void HandleEvent(GraphEventFN f);
};

